## 2.1.0
- DevEco Studio: NEXT Beta1-5.0.3.806,SDK:API12 Release(5.0.0.66)验证通过
## 2.1.0-rc.0
1. 适配DevEco Studio 版本： DevEco Studio NEXT Developer Beta3 (5.0.3.521), OpenHarmony SDK: API12 (5.0.0.25)
2. 适配ComponentV2装饰器

## 2.0.0
1. 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)
2. ArkTs新语法适配


## 1.0.5

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## 1.0.4

- 解决和原生属性命名冲突的问题

## 1.0.3

- api8转化为api9

## 1.0.0

实现功能

1. 类似Material风格的容器
2. 橫向滑动的容器
3. 反向横向滑动的容器
4. 竖向滑动的容器
5. 竖向反向滑动的容器
